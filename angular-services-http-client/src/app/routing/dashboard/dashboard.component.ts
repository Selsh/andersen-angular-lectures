import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { mergeMap } from 'rxjs/operators';

import { UserService } from '../../core/services/user.service';
import { UserModel } from '../../core/models/user.model';

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    public user: UserModel;
    private userId: number;

    constructor(
        private userService: UserService,
        private activateRoute: ActivatedRoute
    ) {
    }

    public ngOnInit(): void {
        this.activateRoute.params
            .pipe(
                mergeMap((params: Params) => {
                    if (!Number(params['userId'])) {
                        this.userId = 1;
                    } else {
                        this.userId = Number(params['userId']);
                    }
                    return this.userService.get(this.userId, UserModel);
                })
            )
            .subscribe((user: UserModel) => {
                this.user = user;
            });
    }
}

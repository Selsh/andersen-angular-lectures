import { Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';
import { SharedModule } from '../shared/shared.module';
import { UserService } from '../core/services/user.service';

export const appRoutes: Routes = [
    { path: '', component: MainComponent },
    { path: 'dashboard', component: DashboardComponent},
    { path: 'dashboard/:userId', component: DashboardComponent},
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        DashboardComponent,
        NotFoundComponent,
        MainComponent
    ],
    providers: [
        UserService
    ]
})
export class RoutingModule {
}

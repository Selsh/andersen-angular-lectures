import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'live',
  templateUrl: 'live.component.html'
})
export class LiveComponent implements OnInit {
  constructor() {
    console.log('Call: constructor');
  }

  ngOnInit() {
    console.log('Call: onInit');
  }
}
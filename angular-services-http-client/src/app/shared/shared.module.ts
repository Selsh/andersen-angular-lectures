import { NgModule } from '@angular/core';

import { ModelWindowComponent } from './model-window/model-window.component';
import { ControlsComponent } from './controls/controls.component';
import { LiveComponent } from './live-component/live.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { ChildComponent } from './child/child.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
    declarations: [
        ListUsersComponent,
        ChildComponent,
        ControlsComponent,
        LiveComponent,
        ModelWindowComponent
    ],
    exports: [
        ListUsersComponent,
        ChildComponent,
        ControlsComponent,
        LiveComponent,
        ModelWindowComponent
    ]
})
export class SharedModule {}

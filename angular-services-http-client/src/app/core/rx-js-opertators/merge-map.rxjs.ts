import {fromEvent} from "rxjs/observable/fromEvent";
import {Observable} from "rxjs/Observable";
import {map, mergeMap} from "rxjs/operators";
import {httpRequest} from "../http-request";

import {RequestMethodEnum} from "../enums/request-method.enum";

(function () {
    const domain: string = 'https://jsonplaceholder.typicode.com/posts/1/';
    const select: HTMLSelectElement = document.querySelector('#entities');

    const observable: Observable<any> = fromEvent(select, 'change');
    observable
        .pipe(
            mergeMap((event: Event) => {
                return httpRequest(RequestMethodEnum.GET, domain + (<HTMLSelectElement>event.target).value);
            }),
            map((response: string) => JSON.parse(response))
        )
        .subscribe((arrayEntity: Array<any>) => console.log(arrayEntity));
})();

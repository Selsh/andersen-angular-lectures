import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { Model } from '../models/model';
import { IDictionary } from '../dictionary/dictionary.interface';
import { PrimitiveType } from '../types/primitive.type';

/**
 * AbstractService - a typical abstract service implementation class
 */
@Injectable()
export abstract class AbstractRestService<T extends Model> {
    /**
     * API endpoint
     */
    private static API_URL: string = 'https://jsonplaceholder.typicode.com/';

  /**
   * @param httpClient
   */
  public constructor(
      protected httpClient: HttpClient
    ) {}

    /**
     * Abstract method return entity path url of REST resource
     * @return {string} url - entity path
     */
    public abstract getEntityPath(): string;

    /**
     * GET method - getAll
     *
     * @param {Model} modelCreator
     * @param {IDictionary<string | number>} queryParams - attach query parameters
     *
     * @return {Observable<Array<Model>>} res - transformed response object
     */
    public getAll(
        modelCreator: { new (data: object): T; },
        queryParams: IDictionary<string | number> = {},
    ): Observable<Array<T>> {
        const params: HttpParams = this.appendDictionaryToURLSearchParams(queryParams);

        const headers: HttpHeaders = new HttpHeaders().append('sdfkdfkgj', 'sdfkdfkgj');
        return this.httpClient.get(this.getUrl(), { params, headers })
            .pipe(
                map((entities: Array<object>) => entities.map((entity: object) => new modelCreator(entity)))
            );
    }

    /**
     * GET method - get
     *
     * @param {number} id - Entity Id
     * @param {Model} modelCreator
     * @param {Object} queryParams - attach query parameters
     * @param {boolean} isShowSpinner
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public get(
        id: number,
        modelCreator: { new (data: IDictionary<PrimitiveType>): T; },
        queryParams: IDictionary<string | number> = {},
        isShowSpinner: boolean = true
    ): Observable<T> {
        const params: HttpParams = this.appendDictionaryToURLSearchParams(queryParams);

        return this.httpClient.get(this.getUrl() + '/' + id, { params })
            .pipe(
                map((entity: IDictionary<PrimitiveType>) => new modelCreator(entity))
            );
    }

    /**
     * POST method - create
     *
     * @param {Model} item - object
     * @param {Model} modelCreator
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public create(item: T, modelCreator: { new (data: object): T; }): Observable<Model> {
        return this.httpClient.post(this.getUrl(), item)
            .pipe(
                map((entity: object) => new modelCreator(entity))
            );
    }

    /**
     * POST|PUT method - save
     *
     * @param {Model} item - object
     * @param {Model} modelCreator
     * @param {boolean} isShowSpinner
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public save(item: T, modelCreator: { new (data: object): T; }, isShowSpinner: boolean = true): Observable<Model> {
        return this.httpClient.put(`${ this.getUrl() }/${ item.id }`, item)
            .pipe(
                map((entity: object) => new modelCreator(entity))
            );
    }

    /**
     * DELETE method - delete
     *
     * @param {number} id - Entity Id
     *
     * @return {Observable<Response>} res - transformed response object
     */
    public delete(id: number): Observable<any> {
        return this.httpClient.delete(`${ this.getUrl() }/${ id }`);
    }

    /**
     * Return the base URL of REST resource
     *
     * @return {string} url - base URL
     */
    public getUrl(): string {
        return AbstractRestService.API_URL + this.getEntityPath();
    }

    /**
     * Appends all key-value pairs into a new URLSearchParams object
     *
     * @return {URLSearchParams}
     */
    protected appendDictionaryToURLSearchParams(dictionary: IDictionary<string | number>): HttpParams {
        const searchParams: HttpParams = new HttpParams();

        for (const key of Object.keys(dictionary)) {
            searchParams.append(key, dictionary[key].toString());
        }

        return searchParams;
    }
}

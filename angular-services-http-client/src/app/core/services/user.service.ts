import { Injectable } from '@angular/core';
import {AbstractRestService} from "./abstract-rest.service";
import {UserModel} from "../models/user.model";

@Injectable()
export class UserService extends AbstractRestService<UserModel> {
    public getEntityPath(): string {
        return "users";
    }
}

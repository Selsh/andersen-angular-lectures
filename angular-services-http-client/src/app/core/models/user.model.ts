import {Model} from "./model";

/**
 * User model
 */
export class UserModel extends Model {
    public name: string;
    public username: string;
    public email: string;
    public phone: string;
    public website?: string;

    public getFullName(): string {
        return `@${ this.username } ${ this.name }`;
    }
}

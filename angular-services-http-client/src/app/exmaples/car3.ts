export class Car {

    public constructor(
        private engine: Engine,
        private tires: Tires
    ) {
    }
}

class Engine {

    public constructor(
        private cylinders: number
    ) {
    }
}

class Tires {
    private brend: string

    public static create(): Tires {
        return new Tires();
    }
}

class CarFactory {
    static createCar(cylinders: number): Car {
        const engine: Engine = new Engine(cylinders);
        const tires: Tires = Tires.create();
        return new Car(engine, tires);
    }
}

const car1: Car = CarFactory.createCar(20);
const car2: Car = CarFactory.createCar(15);

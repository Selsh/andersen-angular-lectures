import { Component, OnInit } from '@angular/core';

import { UserModel } from '../../core/models/user.model';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'list-users',
  styles: [ `
    h1.title {
        color: red;
    }
  ` ],
  templateUrl: 'list-users.component.html'
})
export class ListUsersComponent implements OnInit {
  private userService: UserService = new UserService();
  public users: Array<UserModel> = [];

  public ngOnInit(): void {
    this.loadData();
  }

  private loadData(): void {
    this.userService
      .getAll(UserModel)
      .subscribe((users: Array<UserModel>) => this.users = users);
  }
}

import { Component, ContentChild, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'controls',
  templateUrl: './controls.component.html'
})
export class ControlsComponent {
  @Input() public noIncrement: boolean = false;
  @Input() public noDecrement: boolean = false;
  @Output() public onChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ContentChild('incrementButton') public incrementButton: ElementRef;

  public change(value: boolean): void {
    // console.log(this.incrementButton);
    this.onChanged.emit(value);
  }
}

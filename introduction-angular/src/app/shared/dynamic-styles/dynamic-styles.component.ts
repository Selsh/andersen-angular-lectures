import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'dynamic-styles',
    styleUrls: [ 'dynamic-styles.component.css' ],
    templateUrl: 'dynamic-styles.component.html'
})
export class DynamicStylesComponent {
    public cssClassesP: object = {
        'paragraph': true,
        'paragraph_red': false
    };
    public titleH3Styles: Array<string> = [];

    public onToggleClasses(): void {
        this.cssClassesP['paragraph_red'] = !this.cssClassesP['paragraph_red'];
    }

    public addIconForTitle(): void {
        this.titleH3Styles.push('title-icon');
    }

    public transformUppercase(): void {
        this.titleH3Styles.push('uppercase');
    }

    public resetStyles(): void {
        this.titleH3Styles = [];
    }
}

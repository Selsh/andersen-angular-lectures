import { Component, ViewChild } from '@angular/core';
import { ControlsComponent } from './shared/controls/controls.component';

@Component({
  selector: 'app',
  styleUrls: [],
  templateUrl: './app.component.html'
})
export class AppComponent {
  public title: string = 'Hello, World!';
  public defaultInputValue: string = 'World';
  public name: string = 'Tom';
  public counter: number = 0;
  @ViewChild(ControlsComponent) private controlsComponent: ControlsComponent;

  public increment(): void {
    this.counter++;
  }

  public showCounter(): number {
    return this.counter;
  }

  public changeCounter(value: boolean) {
    value ? this.counter++ : this.counter--;
  }
}

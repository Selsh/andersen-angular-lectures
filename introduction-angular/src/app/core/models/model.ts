import {IDictionary} from "../dictionary/dictionary.interface";
import {PrimitiveType} from "../types/primitive.type";

export class Model {
    id: number;

    public constructor(params?: IDictionary<PrimitiveType>) {
        for (const prop in params) {
            (<any>this)[prop] = params[prop];
        }
    }
}
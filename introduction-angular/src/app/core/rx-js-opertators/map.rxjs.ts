import {Observable} from "rxjs/Observable";
import {range} from "rxjs/internal/observable/range";
import {map} from "rxjs/operators";

(function () {
    const observable: Observable<number> = range(1, 10);
    observable
        .pipe(
            map((value: number) => value * -1)
        )
        .subscribe((value: number) => console.log('Next: ', value)); // -1,-2 ... -10
})();

import {Observer} from "rxjs/Observer";
import {Observable} from "rxjs/Observable";

import { RequestMethodEnum } from "./enums/request-method.enum";

/**
 * XMLHttpRequest wrapper
 *
 * @param {RequestMethodEnum} typeQuery
 * @param {string} url
 * @param {object} body
 * @return {Observable<string>}
 */
export function httpRequest(typeQuery: RequestMethodEnum, url: string, body: object = {}): Observable<string> {
    return Observable.create(
        (observer: Observer<string>) => {
            const xhr: XMLHttpRequest = new XMLHttpRequest();
            xhr.open(typeQuery, url, true);
            xhr.send(body);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    observer.next(xhr.responseText);
                }
            }
        }
    );
}

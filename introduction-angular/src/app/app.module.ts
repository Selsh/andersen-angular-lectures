import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ChildComponent } from './shared/child/child.component';
import { ControlsComponent } from './shared/controls/controls.component';
import { ListUsersComponent } from './shared/list-users/list-users.component';
import {DynamicStylesComponent} from "./shared/dynamic-styles/dynamic-styles.component";

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ListUsersComponent,
    ChildComponent,
    ControlsComponent,
    DynamicStylesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ]
})
export class AppModule {}

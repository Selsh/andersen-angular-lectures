function LogOutput(target: Function, key: string, descriptor: any) {
  const originalMethod = descriptor.value;
  const newMethod = function(...args: any[]): any {
    const result: any = originalMethod.apply(this, args);
    if (!this.loggedOutput) {
      this.loggedOutput = new Array<any>();
    }
    this.loggedOutput.push({
      method: key,
      parameters: args,
      output: result,
      timestamp: new Date()
    });
    return result;
  };
  descriptor.value = newMethod;
}

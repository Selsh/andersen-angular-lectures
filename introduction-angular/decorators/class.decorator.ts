function Greeter(target: Function): void {
  target.prototype.greet = function(): void {
    console.log('Hello!');
  }
}

@Greeter
class Greeting {
  public name: string;

  constructor(name: string = 'No name') {
    this.name = name;
  }
}

const myGreeting = new Greeting();
myGreeting['greet']();

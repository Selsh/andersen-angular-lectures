function LogChanges(callbackObject: any): Function {
  return function(target: Object, key: string): void {
    let propertyValue: string = this[key]; // this ссылается на экземпляр класса
    if (delete this[key]) {
      Object.defineProperty (target, key, {
        get: function() {
          return propertyValue;
        },
        set: function(newValue) {
          propertyValue = newValue;
          callbackObject.onchange.call(this, propertyValue);
        }
      });
    }
  }
}

class Fruit {
  @LogChanges({
    onchange: function(newValue: string): void {
      console.log(`The fruit is ${newValue} now`);
    }
  })
  name: string;
}
var fruit = new Fruit();
fruit.name = 'banana1'; // консоль: 'The fruit is banana now'
fruit.name = 'plantain'; // консоль: 'The fruit is plantain now'
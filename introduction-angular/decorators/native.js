function logDecorator(func) {
	return function () {
		const returnValueFunc = func.apply(this, arguments);
		console.log(`Return: ${ returnValueFunc }`);
		return returnValueFunc;
	};
}

function counterCallDecorator(func) {
	let counter = 0;
	return function () {
		console.log(`Call counter function: ${ ++counter }`);
		return func.apply(this, arguments);
	};
}

let pow = logDecorator(Math.pow);
pow = counterCallDecorator(pow);

pow(2, 2);
pow(2, 3);
pow(2, 4);
pow(2, 5);

const is: boolean = true;
const text: string = 'lorem';
const pi: number = 1;

let response: any = 'sdfg';
response = true;
response = {};

const lorem: any = 'asdasd';

const array: Array<string> = ['asd', 'asd'];
// const array1: string[] = [456, 'asd'];

// array.push(234);

const matrix: number[][] = [
    [ 1, 2, 3],
    [ 1, 2, 3],
    [ 1, 2, 3],
];

const obj: object = {
    a: 1,
    h: 'asdasd'
};

interface IPoint {
    x: number;
    y: number;
}

const point: IPoint = {
    x: 3,
    y: 3
};

interface IzAxis extends IPoint {
    z: number;
}

const point3d: IzAxis = {
    x: 1,
    y: 1,
    z: 1
}

// const name1: string = 1;

function fuc(arg: string, arg1: number = 1, arg2?: number) {
    console.log(arg, arg1, arg2);
}

// fuc('1', 2, 3, 4);



class Unit {
    private _health: number;

    constructor(health) {
        this._health = health;
    }

    public get health(): number {
        return this._health;
    }
}

const unit: Unit = new Unit(2);

abstract class BaseRest {
    private domain: string = 'https://jsonplaceholder.typicode.com';

    public abstract getEntityName(): string;

    public getUrl(): string {
        return [this.domain, this.getEntityName()].join('/');
    }

    public fetchAll(): Promise<Array<any>> {
        return new Promise((resolve: Function, reject: Function) => {
            const xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.open('GET', this.getUrl(), true);

            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(xhr.status);

            xhr.send();
        })
            .then((response: string) => JSON.parse(response))
            .catch((error: Error) => console.error(error));
    }
}

class UserService extends BaseRest {
    public getEntityName(): string {
        return 'users';
    }
}

(new UserService())
    .fetchAll()
    .then((data: Array<any>) => console.log(data))

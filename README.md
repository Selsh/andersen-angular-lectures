# Angular lectures

## Lessons
1. Introduction TypeScript [x]
2. Introduction RxJS [ ]
3. Begin Angular 5 (Modules, Components, Lifecycle hooks) [ ]
4. Reactive Forms and Array Forms [ ]
5. ??? [ ]

## Branches
- typescript-playground - TypeScript builder
- introduction-playground - Examples code
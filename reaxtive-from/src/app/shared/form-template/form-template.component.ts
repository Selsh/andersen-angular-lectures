import { Component, Input } from '@angular/core';
import { NgForm } from "@angular/forms";

import {UserModel} from "../../core/models/user.model";

@Component({
    selector: 'form-template',
    templateUrl: 'form-template.component.html'
})
export class FormTemplateComponent {
    @Input() public user: UserModel = new UserModel();

    public onSubmit(form: NgForm): void {
        if (form.invalid) {
            return;
        }
        console.log('form valid and submit', form.value);
    }
}

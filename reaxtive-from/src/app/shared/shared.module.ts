import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ModelWindowComponent } from './model-window/model-window.component';
import { ControlsComponent } from './controls/controls.component';
import { LiveComponent } from './live-component/live.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { ChildComponent } from './child/child.component';
import { FormTemplateComponent } from "./form-template/form-template.component";
import {FormReactiveComponent} from "./form-reactive/form-reactive.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    declarations: [
        ListUsersComponent,
        ChildComponent,
        ControlsComponent,
        LiveComponent,
        ModelWindowComponent,
        FormTemplateComponent,
        FormReactiveComponent
    ],
    exports: [
        ListUsersComponent,
        ChildComponent,
        ControlsComponent,
        LiveComponent,
        ModelWindowComponent,
        FormTemplateComponent,
        FormReactiveComponent
    ]
})
export class SharedModule {}

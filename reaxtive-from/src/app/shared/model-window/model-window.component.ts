import { Component, Input } from '@angular/core';

@Component({
  selector: 'model-window',
  styleUrls: [ 'model-window.component.css' ],
  templateUrl: 'model-window.component.html'
})
export class ModelWindowComponent {
  @Input() public title: string;
  public isShowModel: boolean = false;

  public open(): void {
    this.isShowModel = true;
  }

  public close(): void {
    this.isShowModel = false;
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

import {UserModel} from "../../core/models/user.model";
import { nameValidator } from "../../core/validators/name.validator";

@Component({
    selector: 'form-reactive',
    templateUrl: 'form-reactive.component.html',
    styleUrls: [ 'form-reactive.component.css' ]
})
export class FormReactiveComponent implements OnInit {
    @Input() public user: UserModel = new UserModel();
    @Output() public onSubmit: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    public form: FormGroup;

    public constructor(
        private formBuilder: FormBuilder
    ) {}

    public ngOnInit(): void {
        this.createForm();
    }

    public submit(): void {
        if (this.form.invalid) {
            return;
        }

        console.log('Submit: ', this.form.value, this.form.controls);
        this.onSubmit.emit(new UserModel(this.form.value));
        this.form.reset();
    }

    public addAddress(): void {
        (<FormArray>this.form.get('addresses')).push(this.createAddress());
    }

    private createForm(): void {
        this.form = this.formBuilder.group({
            name: new FormControl(null, [Validators.required, Validators.minLength(3), nameValidator]),
            username: new FormControl(null, Validators.required),
            email: new FormControl(null, [Validators.required, Validators.email]),
            phone: new FormControl(null, Validators.required),
            website: new FormControl(),
            addresses: this.formBuilder.array([])
        });

        this.form.patchValue(this.user);
    }

    private createAddress(): FormGroup  {
        return this.formBuilder.group({
            street: new FormControl(),
            suite: new FormControl(),
            city: new FormControl(),
            zipcode: new FormControl()
        });
    }
}

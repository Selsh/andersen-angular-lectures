import { Component } from '@angular/core';

@Component({
    selector: 'main',
    templateUrl: 'main.component.html'
})
export class MainComponent {
    public result: object = {};
    constructor() {
    }

    public submit(value: object): void {
        this.result = value;
    }
}
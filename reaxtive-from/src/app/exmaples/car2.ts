export class Car {

    public constructor(
        private engine: Engine,
        private tires: Tires
    ) {
    }
}

class Engine {

    public constructor(
        private cylinders: number
    ) {
    }
}

class Tires {
    private brend: string

    public static create(): Tires {
        return new Tires();
    }
}

const car1: Car = new Car(new Engine(20), Tires.create());
const car2: Car = new Car(new Engine(15), Tires.create());

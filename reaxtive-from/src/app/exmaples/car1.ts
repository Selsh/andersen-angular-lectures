export class Car {
    private engine: Engine;
    private tires: Tires;

    public constructor() {
        this.engine = new Engine();
        this.tires = Tires.create();
    }
}

class Engine {
    cylinders: number;
}

class Tires {
    private brend: string

    public static create(): Tires {
        return new Tires();
    }
}

const car1: Car = new Car();
const car2: Car = new Car();

import {FormControl} from "@angular/forms";

export function nameValidator({value}: FormControl) {
    if (String(value).split(' ').length === 2) {
        return null;
    } else {
        return { nameValidator: { value: false } };
    }
}
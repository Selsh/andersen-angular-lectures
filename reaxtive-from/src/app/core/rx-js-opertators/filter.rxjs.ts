import {Observable} from "rxjs/Observable";
import {range} from "rxjs/internal/observable/range";
import {filter} from "rxjs/operators";

(function () {
    const observable: Observable<number> = range(1, 10);
    observable
        .pipe(
            filter((value: number) => value % 3 === 0)
        )
        .subscribe((value: number) => console.log(value)); // 3, 6, 9
})();

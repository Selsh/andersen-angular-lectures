/**
 * Primitive types: number, string, boolean
 */
export type PrimitiveType = number | string | boolean;

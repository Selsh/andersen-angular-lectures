import {fromEvent} from "rxjs/observable/fromEvent";
import {debounceTime, map} from "rxjs/operators";
import {Observable} from "rxjs/Observable";

(function () {
    // DOM Event
    const searchInput: HTMLInputElement = document.querySelector('#search');
    const changeInputStream: Observable<Event> = fromEvent(searchInput, 'change');
    changeInputStream
        .pipe(
            debounceTime<Event>(500),
            map((event: Event) => {
                return (<HTMLInputElement>event.target).value;
            })
        )
        .subscribe((value: string) => console.log(value));
})();

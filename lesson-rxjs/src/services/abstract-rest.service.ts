import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {pipe} from "rxjs/internal-compatibility";

import {IDictionary} from "../dictionary/dictionary.interface";
import {Model} from "../models/model";
import {httpRequest} from "../http-request";
import {RequestMethodEnum} from "../enums/request-method.enum";
import {PrimitiveType} from "../types/primitive.type";

/**
 * AbstractService - a typical abstract service implementation class
 */

export abstract class AbstractRestService<T extends Model> {
    /**
     * API endpoint
     */
    private static API_URL: string = 'https://jsonplaceholder.typicode.com/posts/1/';

    private parseResponse: any = pipe(
        map((response: string) => JSON.parse(response))
    );

    /**
     * Abstract method return entity path url of REST resource
     * @return {string} url - entity path
     */
    public abstract getEntityPath(): string;

    /**
     * GET method - getAll
     *
     * @param {Model} modelCreator
     * @param {IDictionary<string | number>} queryParams - attach query parameters
     *
     * @return {Observable<Array<Model>>} res - transformed response object
     */
    public getAll(
        modelCreator: { new (data: IDictionary<PrimitiveType>): T; },
        queryParams: IDictionary<string | number> = {},
    ): Observable<Array<T>> {
        const params: URLSearchParams = this.appendDictionaryToURLSearchParams(queryParams);

        return httpRequest(RequestMethodEnum.GET, this.getUrl(), { params })
            .pipe(
                this.parseResponse,
                map((entities: Array<IDictionary<PrimitiveType>>) => entities.map((entity) => new modelCreator(entity)))
            );
    }

    /**
     * GET method - get
     *
     * @param {number} id - Entity Id
     * @param {Model} modelCreator
     * @param {Object} queryParams - attach query parameters
     * @param {boolean} isShowSpinner
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public get(
        id: number,
        modelCreator: { new (data: IDictionary<PrimitiveType>): T; },
        queryParams: IDictionary<string | number> = {},
        isShowSpinner: boolean = true
    ): Observable<T> {
        const params: URLSearchParams = this.appendDictionaryToURLSearchParams(queryParams);

        return httpRequest(RequestMethodEnum.GET, this.getUrl() + '/' + id, { params })
            .pipe(this.parseResponse);
    }

    /**
     * POST method - create
     *
     * @param {Model} item - object
     * @param {Model} modelCreator
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public create(item: T, modelCreator: { new (data: Response): T; }): Observable<Model> {
        return httpRequest(RequestMethodEnum.POST, this.getUrl(), item)
            .pipe(
                this.parseResponse
            );
    }

    /**
     * POST|PUT method - save
     *
     * @param {Model} item - object
     * @param {Model} modelCreator
     * @param {boolean} isShowSpinner
     *
     * @return {Observable<Model>} res - transformed response object
     */
    public save(item: T, modelCreator: { new (data: Response): T; }, isShowSpinner: boolean = true): Observable<Model> {
        return httpRequest(RequestMethodEnum.PUT, `${ this.getUrl() }/${ item.id }`, item)
            .pipe(
                this.parseResponse
            );
    }

    /**
     * DELETE method - delete
     *
     * @param {number} id - Entity Id
     *
     * @return {Observable<Response>} res - transformed response object
     */
    public delete(id: number): Observable<Response> {
        return httpRequest(RequestMethodEnum.DELETE, `${ this.getUrl() }/${ id }`)
            .pipe(
                this.parseResponse
            );
    }

    /**
     * Return the base URL of REST resource
     *
     * @return {string} url - base URL
     */
    public getUrl(): string {
        return AbstractRestService.API_URL + this.getEntityPath();
    }

    /**
     * Appends all key-value pairs into a new URLSearchParams object
     *
     * @return {URLSearchParams}
     */
    protected appendDictionaryToURLSearchParams(dictionary: IDictionary<string | number>): URLSearchParams {
        const searchParams: URLSearchParams = new URLSearchParams();

        for (const key of Object.keys(dictionary)) {
            searchParams.append(key, dictionary[key].toString());
        }

        return searchParams;
    }
}
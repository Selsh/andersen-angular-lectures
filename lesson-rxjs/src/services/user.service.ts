import {AbstractRestService} from "./abstract-rest.service";
import {UserModel} from "../models/user.model";

export class UserService extends AbstractRestService<UserModel> {
    public getEntityPath(): string {
        return "users";
    }
}

import {UserService} from "./services/user.service";
import {UserModel} from "./models/user.model";

(function () {
    (new UserService())
        .getAll(UserModel)
        .subscribe((value: Array<UserModel>) => console.log(value));
})();

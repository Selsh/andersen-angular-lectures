// function customReverse<T>(array: T): T {
//     return array.reverse();
// }

function customReverse<T>(array: Array<T>): Array<T> {
    return array.reverse();
}

const reverseArray: Array<number> = customReverse<number>([1, 2, 3]);

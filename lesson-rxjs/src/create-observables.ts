import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";

(function () {
    const stream: Observable<string> = Observable.create(
        (observer: Observer<string>) => {
            observer.next('Hello');
            observer.next('World');
            observer.complete();
            observer.error(new Error());
        }
    );

    stream.subscribe(
        (value: string) => console.log('Next: ' + value),
        (error: Error) => console.error('Error: ', error),
        () => console.info('Complete!')
    );
})();

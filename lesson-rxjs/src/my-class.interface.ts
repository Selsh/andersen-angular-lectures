import {Model} from "./models/model";

interface IMyClass {
    new (params: any): Model;
}

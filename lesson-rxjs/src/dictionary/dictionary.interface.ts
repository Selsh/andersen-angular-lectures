/**
 * IDictionary
 */
export interface IDictionary<T> {
    [key: string]: T | IDictionary<T>;
}

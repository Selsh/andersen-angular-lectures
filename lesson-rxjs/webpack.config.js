module.exports = {
    entry: [
        './src/app.ts',
        './src/create-observables.ts',
        './src/rx-js-opertators/merge-map.rxjs.ts',
        './src/dom-event.ts'
    ],
    output: {
        filename: 'bundle.js',
        path: __dirname
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
};